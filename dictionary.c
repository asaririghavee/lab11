#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
   FILE *in = fopen(filename, "r");
   if (!in)
   {
      perror("Can't open dictionary");
      exit(1);
   }
   int arraylength=10;
   // Allocate memory for an array of strings (arr).
   char **s=malloc(arraylength*sizeof(char *));
   int entries=0;
   char mystring[20];
   // Read the dictionary line by line.
   while(fgets(mystring,20,in) != NULL)
   {  
      char *nl = strchr(mystring, '\n');
      if(nl != NULL)
         *nl = '\0';
      // Expand array if necessary (realloc).
      if(entries==arraylength)
      {
         arraylength+=10;
         s=realloc(s,arraylength*sizeof(char *));
      }
   	  // find the string length and allocate memory for the string (str).
      int slen=strlen(mystring)+1;
      char *lineread=malloc(slen*sizeof(char ));
   	  // Copy each line into the string (use strcpy).
      strcpy(lineread,mystring);
      // Attach the string to the large array (assignment =).
      s[entries]=lineread;
      entries++;
   }
   // The size should be the number of entries in the array.
   *size = entries;
   // Return pointer to the array of strings.
   return s;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
   if (dictionary == NULL) 
      return NULL;
    
   for (int i = 0; i < size; i++)
   {
      if (strcmp(target, dictionary[i]) == 0)
      {
         return dictionary[i];
      }
   }
   return NULL;
}